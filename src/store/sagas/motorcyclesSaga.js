import {takeLatest, call, put} from 'redux-saga/effects';
import axios from 'axios';

import {
  fetchMotorcycles as fetchMotorcyclesAction,
  fetchMotorcyclesStart,
  fetchMotorcyclesSuccess,
  fetchMotorcyclesFailure,
} from '../slices/motorcyclesSlice';

function* fetchMotorcyclesSaga() {
  try {
    yield put(fetchMotorcyclesStart());

    const response = yield call(axios.get, 'https://fake.prod.mapit.me/motos');

    yield put(fetchMotorcyclesSuccess(response.data));
  } catch (error) {
    yield put(fetchMotorcyclesFailure(error.message));
  }
}

export default function* motorcyclesSaga() {
  yield takeLatest(fetchMotorcyclesAction, fetchMotorcyclesSaga);
}
