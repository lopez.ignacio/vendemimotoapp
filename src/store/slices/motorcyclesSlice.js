import { createSlice, createAction} from '@reduxjs/toolkit'

const sliceName = 'motorcycles'

const motorcyclesSlice = createSlice({
  name: 'motorcycles',
  initialState: {
    motorcycles: {
      data: [],
      loading: false,
      error: null,
    },
    motorcycleSelected: {
      data: {}
    }
  },
  reducers: {
    fetchMotorcyclesStart(state) {
      state.motorcycles.loading = true;
      state.motorcycles.error = null;
    },
    fetchMotorcyclesSuccess(state, action) {
      state.motorcycles.data = action.payload;
      state.motorcycles.loading = false;
      state.motorcycles.error = null;
    },
    fetchMotorcyclesFailure(state, action) {
      state.motorcycles.loading = false;
      state.motorcycles.error = action.payload;
    },
    selectedMotorcycle(state, action) {
      state.motorcycleSelected.data = action.payload
    }
  },
});

export const fetchMotorcycles = createAction(`${sliceName}/fetchMotorcycles`)

export const { 
  fetchMotorcyclesStart, 
  fetchMotorcyclesSuccess, 
  fetchMotorcyclesFailure, 
  selectedMotorcycle
} = motorcyclesSlice.actions;

export default motorcyclesSlice.reducer;
