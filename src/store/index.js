import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import motorcyclesReducer from '../store/slices/motorcyclesSlice';
import motorcyclesSaga from '../store/sagas/motorcyclesSaga';

function* rootSaga() {
  yield all([motorcyclesSaga()]);
}

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    motorcycles: motorcyclesReducer,
  },
  middleware: [sagaMiddleware],
});

sagaMiddleware.run(rootSaga);

export default store;
