import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import homeStyles from './HomeScreen.styles';
import {
  fetchMotorcycles,
  selectedMotorcycle,
} from '../../store/slices/motorcyclesSlice';
import {MOTORCYCLE_DETAIL_SCREEN} from '../../../App';

const Home = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const {data, loading} = useSelector(state => state.motorcycles.motorcycles);

  useEffect(() => {
    dispatch(fetchMotorcycles());
  }, []);

  const renderItem = ({item, index}) => (
    <TouchableOpacity
      onPress={() => handleGoToMap(item)}
      style={[homeStyles.button]}>
      <Text style={homeStyles.itemText}>{item.id}</Text>
    </TouchableOpacity>
  );

  const handleGoToMap = selectedItem => {
    navigation.navigate(MOTORCYCLE_DETAIL_SCREEN, {motorcycle: selectedItem});
    dispatch(selectedMotorcycle(selectedItem));
  };

  return (
    <SafeAreaView style={homeStyles.container}>
      <View>
        <Text style={homeStyles.text}>Listado de motos</Text>
        {loading ? (
          <ActivityIndicator
            size="large"
            color="#0000ff"
            style={homeStyles.loadingIndicator}
          />
        ) : (
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default Home;
