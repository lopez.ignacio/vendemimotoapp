import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
    alignContent: 'space-between',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 20,
  },
  button: {
    marginTop: 5,
    borderRadius: 10,
    backgroundColor: '#2196F3',
    padding: 15,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    padding: 12,
    marginBottom: 8,
  },
  itemIcon: {
    marginRight: 8,
  },
  itemText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  loadingIndicator: {
    marginTop: 20,
  },
});

export default styles;
