export const calculateRepurchaseValue = (purchaseValue, purchaseDate) => {
  const yearElapsed = diffYears(purchaseDate)

  // Verificar si ha pasado al menos un año desde la fecha de compra
  if (yearElapsed >= 1) {
    //Calcula el valor de recompra dividiendo el precio de compra por 2 elevado a la potencia de los años transcurridos. 
    // Esto implica dividir el precio de compra a la mitad por cada año completo transcurrido.
    const repurchaseValue = purchaseValue / Math.pow(2, yearElapsed);
    return repurchaseValue.toFixed(1);
  }

  return purchaseValue.toFixed(2);
};

const diffYears = (purchaseDate) => {
  const currentDate = new Date();
  purchaseDate = new Date(purchaseDate)

  // Calcula la diferencia en milisegundos entre la fecha actual y la fecha de compra. 
  // Esto se hace restando el valor devuelto por getTime() de ambas fechas. 
  // El resultado es el tiempo transcurrido en milisegundos.
  const timeElapsed = currentDate.getTime() - purchaseDate.getTime();

  // Calcula el número de años transcurridos redondeando hacia abajo. 
  // Dividimos el tiempo transcurrido en milisegundos por la cantidad de milisegundos en un año 
  // (365 días * 24 horas * 60 minutos * 60 segundos * 1000 milisegundos). Usamos Math.floor() 
  // para asegurarnos de obtener un número entero.
  const yearElapsed = Math.floor(timeElapsed / (1000 * 60 * 60 * 24 * 365));

  return yearElapsed;
}