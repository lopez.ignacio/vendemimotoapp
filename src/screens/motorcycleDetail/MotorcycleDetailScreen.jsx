import React, {useState} from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import motorcycleDetailStyles from './MotorcycleDetailScreen.styles';
import {calculateRepurchaseValue} from './helpers/utils';
import MapMotorcycle from './components/mapMotorcycle/MapMotorcycle';
import ModalMotorycle from './components/modalMotorcycle/ModalMotorcycle';
import DescriptionMotorcycle from './components/descriptionMotorcycle/DescriptionMotorcycle';

const MotorcycleDetailScreen = () => {
  const {data} = useSelector(state => state.motorcycles.motorcycleSelected);
  const latitude = data.coordenadas.latitud;
  const longitude = data.coordenadas.longitud;
  const purchaseDate = data.fechaCompra;
  const purchaseValue = data.precioCompra;
  const model = data.modelo;
  const repurchaseValue = calculateRepurchaseValue(purchaseValue, purchaseDate);
  const [modalVisible, setModalVisible] = useState(false);

  const handleRequestAppointment = () => {
    setModalVisible(true);
  };

  const handleModalClose = () => {
    setModalVisible(false);
  };

  return (
    <View style={motorcycleDetailStyles.container}>
      <Text style={motorcycleDetailStyles.title}>{data.id}</Text>
      <ScrollView contentContainerStyle={motorcycleDetailStyles.scrollView}>
        <MapMotorcycle {...{latitude, longitude}} />
        <DescriptionMotorcycle {...{model, repurchaseValue}} />
      </ScrollView>

      <ModalMotorycle
        isVisible={modalVisible}
        close={handleModalClose}></ModalMotorycle>

      <TouchableOpacity
        style={motorcycleDetailStyles.button}
        onPress={handleRequestAppointment}>
        <Text style={motorcycleDetailStyles.textButton}>Solicitar Cita</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MotorcycleDetailScreen;
