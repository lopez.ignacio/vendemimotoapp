import React from 'react';
import MapView, {Marker} from 'react-native-maps';
import mapMotorcycleStyles from './MapMotorcycle.styles';

const MapMotorcycle = ({latitude, longitude}) => {
  return (
    <MapView
      style={mapMotorcycleStyles.map}
      initialRegion={{
        latitude,
        longitude,
        latitudeDelta: '0.5',
        longitudeDelta: '0.16875',
      }}>
      <Marker
        coordinate={{latitude, longitude}}
        title="Ubicación"
        description={`Coordenadas: ${latitude}, ${longitude}`}
      />
    </MapView>
  );
};

export default MapMotorcycle;
