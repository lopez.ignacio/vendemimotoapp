import React from 'react';
import {Button, Modal, Text, View} from 'react-native';
import modalMotorcycleStyles from './ModalMotorcycle.styles';

const ModalMotorycle = ({isVisible, close}) => {
  return (
    <Modal
      visible={isVisible}
      animationType="slide"
      transparent={true}
      onRequestClose={close}>
      <View style={modalMotorcycleStyles.container}>
        <View style={modalMotorcycleStyles.content}>
          <Text style={modalMotorcycleStyles.title}>Cita Solicitada</Text>
          <Text style={modalMotorcycleStyles.text}>
            Su concesionario se pondrá en contacto pronto, para concretar cita
          </Text>
          <Button title="Cerrar" onPress={close} />
        </View>
      </View>
    </Modal>
  );
};

export default ModalMotorycle;
