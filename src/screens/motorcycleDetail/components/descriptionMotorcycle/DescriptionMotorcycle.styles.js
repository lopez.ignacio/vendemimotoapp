import { StyleSheet } from 'react-native'; 

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    marginBottom: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
})

export default styles