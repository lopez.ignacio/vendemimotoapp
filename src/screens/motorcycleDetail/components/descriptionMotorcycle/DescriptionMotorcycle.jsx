import React from 'react';
import {Text} from 'react-native';
import descriptionMotorcycleStyles from './DescriptionMotorcycle.styles';

const DescriptionMotorcycle = ({model, repurchaseValue}) => {
  return (
    <>
      <Text style={descriptionMotorcycleStyles.text}>
        El valor mostrado a continuación es una estimación de precio de recompra
        aproximado, es necesario realizar una tasación en un taller. Por favor,
        consulta con tu consecionario para obtener una oferta en firme.
      </Text>
      <Text style={descriptionMotorcycleStyles.text}>
        El valor calculado de recompra de su motocicleta{' '}
        <Text style={descriptionMotorcycleStyles.bold}>{model}</Text> es de{' '}
        <Text style={descriptionMotorcycleStyles.bold}>{repurchaseValue}</Text>{' '}
        €
      </Text>
    </>
  );
};

export default DescriptionMotorcycle;
