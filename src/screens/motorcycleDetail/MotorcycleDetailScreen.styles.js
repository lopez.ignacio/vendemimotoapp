import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 10,
  },
  contescrollViewnt: {
    flexGrow: 1,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  button: {
    borderRadius: 10,
    backgroundColor: '#2196F3',
    padding: 15,
  },
  textButton: {
    fontSize: 14,
    textAlign: 'center',
    color: 'white',
  },
});

export default styles;
