## Vende Mi Moto APP

<img src="./assets/screenshots/screen1.png" width="280" height="520"/>
<img src="./assets/screenshots/screen2.png" width="280" height="520"/>
<img src="./assets/screenshots/screen3.png" width="280" height="520"/>

### En esta aplicación de test se han utilizado las siguientes librerías:

- "axios": Para la interactuar con el servicio API de https://fake.prod.mapit.me/motos
- "react-native-maps": Para poder visualizar la ubicacion de la moto
- "reduxjs/toolkit": Arquitectura para separar UI con la logica de negocio (https://redux-toolkit.js.org/)
- "react-native-safe-area-context": Para que se pueda visualizar las pantallas correctamente en dispositivos que contienen notch (iPhone)

### Secuencia y contenido de la app:

- Al abrirse la app se obtendrá el listado de motos obteniendo los datos de la API correspondiente
- Al hacer click en algunas de ellas, se redireccionará a la pantalla de detalle de la moto
- En la pantalla del detalle se visualizara la localizacion en donde se encuentra la moto
- También se obtendra el valor de recompra segun el calculo indicado
- Habrá un boton de Solicitar cita donde se mostrara un modal confirmando la misma

### Arquitectura y organización de carpetas:

- screens: Archivos exclusivamente de UI
- screens/components: Componentes necesarios que requieran ser creados para optimizar el codigo y generar una mejor visibilidad
- screens/helpers: Carpeta donde se vuelcan los archivos que son de utilidad en lo largo del proyecto.
- store: Carpeta que contendra los slices y sagas para poder manejar los estados e interactuar con la API
- store/sagas: Archivos en donde funcionara como middleware entre la UI y la API, para poder interaccionar con ella y como resultado, gestionar estados.
- store/slices: Archivos en donde se declarara los posibles estados de una accion determinada.
