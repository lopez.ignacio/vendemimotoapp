import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store/index';

import Home from './src/screens/home/HomeScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {NavigationContainer} from '@react-navigation/native';
import MotorcycleDetailScreen from './src/screens/motorcycleDetail/MotorcycleDetailScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer initialRouteName="HomeScreen">
        <Stack.Navigator>
          <Stack.Screen
            name="HomeScreen"
            component={Home}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="MotorcycleDetailScreen"
            options={{headerTitle: 'Detalle'}}
            component={MotorcycleDetailScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export const HOME_SCREEN = 'HomeScreen';
export const MOTORCYCLE_DETAIL_SCREEN = 'MotorcycleDetailScreen';
